package com.example.appsotayenglish.ui.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.appsotayenglish.R;
import com.example.appsotayenglish.data.adapter.AdapterCategory;
import com.example.appsotayenglish.data.local.CategoryViewModel;
import com.example.appsotayenglish.data.model.api.CategoryResponse;
import com.example.appsotayenglish.data.model.room.CategoryRoom;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.List;


public class PushDataRoom extends AppCompatActivity {

    private CategoryResponse categoryResponse = new CategoryResponse();
    private List<CategoryRoom> categoryRooms = new ArrayList<>();
    public static final int ADD_NOTE_REQUEST = 1;
    private CategoryViewModel categoryViewModel;
    private RecyclerView recyclerView_category;
    private AdapterCategory adapterCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_data_room);

        FloatingActionButton fab = findViewById(R.id.button_add_note);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PushDataRoom.this, AddCategoryActivity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);
            }
        });
        recyclerView_category = findViewById(R.id.recycler_view);
        recyclerView_category.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        recyclerView_category.setHasFixedSize(true);


        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);

        categoryViewModel.getAllCategory().observe(this, new Observer<List<CategoryRoom>>() {
            @Override
            public void onChanged(List<CategoryRoom> categoryRooms) {
                adapterCategory = new AdapterCategory(getApplicationContext(), categoryRooms);
                recyclerView_category.setAdapter(adapterCategory);

//                for (CategoryRoom categoryRoom : categoryRooms) {
//                    Log.d("id", categoryRoom.getId() + "");
//                    Log.d("name", categoryRoom.getName() + "");
//                    Log.d("img", categoryRoom.getImage() + "");
//                    Log.d("=====================", "=============");
//            }
            }
        });

        //   readFile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            Toast.makeText(this, "Category saved", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Category not saved", Toast.LENGTH_SHORT).show();
        }
    }


//    private void readFile() {
//        MediaPlayer mediaPlayer = new MediaPlayer();
//        String path = Environment.getExternalStorageDirectory().getPath() + "/mesa/1";
//        Log.d("Files", "Path: " + path);
//        File f = new File(path);
//        File file[] = f.listFiles();
//        Log.d("Files", "Size: " + file.length);
//        for (int i = 0; i < file.length; i++) {
//            //here populate your listview
//            Log.d("Files", "FileName:" + file[i].getName());
//          //  String urlMp3 = "file:///storage/emulated/0/mesa/1/1_Có cháy kìa!/" + file[0].getName();
//          //  Log.d("LINKMP3", "link: " + urlMp3);
//          //  Uri myUri = Uri.parse(urlMp3);
////            try {
////                mediaPlayer.setDataSource(this, myUri);
////                mediaPlayer.prepare();
////                mediaPlayer.start();
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
//            if (!file.toString().equals("mesa")) {
//            }
//        }
//    }
}
