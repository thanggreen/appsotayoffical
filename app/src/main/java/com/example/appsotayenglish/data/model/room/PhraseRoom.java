package com.example.appsotayenglish.data.model.room;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.appsotayenglish.data.model.api.Translate;

@Entity(tableName = "phrase_room")
public class PhraseRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int category_id;
    private String name;
    private int favorite_status;

    public PhraseRoom(int id, String name, int category_id, int favorite_status) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.favorite_status = favorite_status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(int favorite_status) {
        this.favorite_status = favorite_status;
    }
}
