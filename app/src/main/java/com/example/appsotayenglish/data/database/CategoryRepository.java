package com.example.appsotayenglish.data.database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.appsotayenglish.data.local.CategoryDAO;
import com.example.appsotayenglish.data.model.room.CategoryRoom;

import java.util.List;

public class CategoryRepository {
    private CategoryDAO categoryDAO;
    private LiveData<List<CategoryRoom>> allCategory;

    public CategoryRepository(Application application){
        CategoryDatabase categoryDatabase = CategoryDatabase.getInstance(application);
        categoryDAO = categoryDatabase.categoryDAO();
        allCategory = categoryDAO.getAllCategory();
    }

    public void insert(CategoryRoom categoryRoom){
        new InsertCategoryAsyncTask(categoryDAO).execute(categoryRoom);
    }

    public void destroyAll(){
        new DeleteAllCategoriesAsyncTask(categoryDAO).execute();
    }

    public LiveData<List<CategoryRoom>> getAllCategory(){
        return allCategory;
    }

    public static class InsertCategoryAsyncTask extends AsyncTask<CategoryRoom,
            Void, Void>{
        private CategoryDAO categoryDAO;

        public InsertCategoryAsyncTask(CategoryDAO categoryDAO) {
            this.categoryDAO = categoryDAO;
        }

        @Override
        protected Void doInBackground(CategoryRoom... categoryRooms) {
            categoryDAO.insertCategory(categoryRooms[0]);
            return null;
        }
    }

    private static class DeleteAllCategoriesAsyncTask  extends AsyncTask<Void, Void, Void>{
        private CategoryDAO categoryDAO;

        private DeleteAllCategoriesAsyncTask (CategoryDAO categoryDAO){
            this.categoryDAO = categoryDAO;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            categoryDAO.destroyAll();
            return null;
        }
    }
}
