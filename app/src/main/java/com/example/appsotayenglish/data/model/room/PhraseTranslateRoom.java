package com.example.appsotayenglish.data.model.room;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "phrase_translate_room")
public class PhraseTranslateRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int language_id;
    private int phrase_id;
    private String sound;
    private String translate;

    public PhraseTranslateRoom(int id, int language_id, int phrase_id, String sound, String translate) {
        this.id = id;
        this.language_id = language_id;
        this.phrase_id = phrase_id;
        this.sound = sound;
        this.translate = translate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public int getPhrase_id() {
        return phrase_id;
    }

    public void setPhrase_id(int phrase_id) {
        this.phrase_id = phrase_id;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }
}
