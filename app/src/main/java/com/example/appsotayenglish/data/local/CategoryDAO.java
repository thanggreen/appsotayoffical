package com.example.appsotayenglish.data.local;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.appsotayenglish.data.model.room.CategoryRoom;

import java.util.List;


@Dao
public interface CategoryDAO {

    @Insert
    void insertCategory(CategoryRoom categoryRoom);

    @Query("DELETE FROM category_table")
    void destroyAll();

    @Query("SELECT * FROM category_table")
    LiveData<List<CategoryRoom>> getAllCategory();
}
