package com.example.appsotayenglish.data.model.room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "language_room")
public class LanguageRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String code;
    private String image;

    public LanguageRoom(int id, String code, String image) {
        this.id = id;
        this.code = code;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
