package com.example.appsotayenglish.ui.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.appsotayenglish.R;

public class LogoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
    }
}
