package com.example.appsotayenglish.data.local;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.example.appsotayenglish.data.database.CategoryRepository;
import com.example.appsotayenglish.data.model.room.CategoryRoom;

import java.util.List;

public class CategoryViewModel extends AndroidViewModel {
    private CategoryRepository categoryRepository;
    private LiveData<List<CategoryRoom>> allCategory;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
        categoryRepository = new CategoryRepository(application);
        allCategory = categoryRepository.getAllCategory();
    }

    public void insert(CategoryRoom categoryRoom){
        categoryRepository.insert(categoryRoom);
    }


    public void destroyAll(){
        categoryRepository.destroyAll();
    }



    public LiveData<List<CategoryRoom>> getAllCategory(){
        return allCategory;
    }
}
