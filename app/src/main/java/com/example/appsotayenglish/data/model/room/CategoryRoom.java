package com.example.appsotayenglish.data.model.room;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.appsotayenglish.data.model.api.Phrase;
import java.util.List;

@Entity(tableName = "category_table")
public class CategoryRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String image;

    public CategoryRoom(String name, String image) {

        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
