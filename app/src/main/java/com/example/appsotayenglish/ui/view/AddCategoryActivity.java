package com.example.appsotayenglish.ui.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.example.appsotayenglish.R;

public class AddCategoryActivity extends AppCompatActivity {

    public static final String CATEGORY_ID =
            "CATEGORY_ID";
    public static final String CATEGORY_NAME =
            "CATEGORY_NAME";
    public static final String CATEGORY_IMAGE =
            "CATEGORY_IMAGE";

    private EditText edtName;
    private EditText edtImage;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        edtName = findViewById(R.id.edtname);
        edtImage = findViewById(R.id.edtimage);
        btnSave = findViewById(R.id.btnInsert);

        Intent intent = getIntent();

        if (intent.hasExtra(CATEGORY_ID)) {
            setTitle("Add Note");
            edtName.setText(intent.getStringExtra(CATEGORY_NAME));
            edtImage.setText(intent.getStringExtra(CATEGORY_IMAGE));

        } else {
           // setTitle("Add Note");
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNote();
            }
        });
    }

    private void saveNote() {
        String name = edtName.getText().toString();
        String image = edtImage.getText().toString();

        if (name.trim().isEmpty() || image.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a name and image", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(CATEGORY_NAME, name);
        data.putExtra(CATEGORY_IMAGE, image);

        int id = getIntent().getIntExtra(CATEGORY_ID, -1);
        if (id != -1) {
            data.putExtra(CATEGORY_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }
}
