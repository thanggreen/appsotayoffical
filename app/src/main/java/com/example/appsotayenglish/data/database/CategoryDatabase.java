package com.example.appsotayenglish.data.database;

import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.example.appsotayenglish.data.local.CategoryDAO;
import com.example.appsotayenglish.data.model.room.CategoryRoom;


@Database(entities = {CategoryRoom.class}, version = 1, exportSchema = false)
public abstract class CategoryDatabase extends RoomDatabase{
    private static CategoryDatabase categoryDatabase;

    public abstract CategoryDAO categoryDAO();

    public static synchronized CategoryDatabase getInstance(Context context){
        if(categoryDatabase == null){
            categoryDatabase = Room.databaseBuilder(context.getApplicationContext(),
                    CategoryDatabase.class, "category_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return categoryDatabase;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(categoryDatabase).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void>{
        private CategoryDAO categoryDAO;

        private PopulateDbAsyncTask(CategoryDatabase categoryDatabase){
            categoryDAO = categoryDatabase.categoryDAO();
        }
        @Override
        protected Void doInBackground(Void... voids) {
//            categoryDAO.insertCategory(new CategoryRoom("Title 1", "Description 1"));
//            categoryDAO.insertCategory(new CategoryRoom("Title 2", "Description 2"));
            return null;
        }
    }
}
