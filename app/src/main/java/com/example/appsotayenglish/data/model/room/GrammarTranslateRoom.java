package com.example.appsotayenglish.data.model.room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "grammar_translate_room")
public class GrammarTranslateRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int language_id;
    private int grammar_id;
    private String translate;

    public GrammarTranslateRoom(int id, int language_id, int grammar_id, String translate) {
        this.id = id;
        this.language_id = language_id;
        this.grammar_id = grammar_id;
        this.translate = translate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public int getGrammar_id() {
        return grammar_id;
    }

    public void setGrammar_id(int grammar_id) {
        this.grammar_id = grammar_id;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }
}
