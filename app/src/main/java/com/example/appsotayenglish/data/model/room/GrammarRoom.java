package com.example.appsotayenglish.data.model.room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "grammar_table")
public class GrammarRoom {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private int favorite_status;

    public GrammarRoom(int id, String name, int favorite_status) {
        this.id = id;
        this.name = name;
        this.favorite_status = favorite_status;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFavorite_status() {
        return favorite_status;
    }

    public void setFavorite_status(int favorite_status) {
        this.favorite_status = favorite_status;
    }

}
