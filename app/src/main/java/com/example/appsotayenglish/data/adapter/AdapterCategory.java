package com.example.appsotayenglish.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.appsotayenglish.R;
import com.example.appsotayenglish.data.model.room.CategoryRoom;

import java.util.List;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.HolderAddCategory>{

    Context context;
    LayoutInflater layoutInflater;
    List<CategoryRoom> lstCategory;

    public AdapterCategory(Context context, List<CategoryRoom> lstCategory) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.lstCategory = lstCategory;
    }

    @NonNull
    @Override
    public HolderAddCategory onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_category, parent, false);
        return new AdapterCategory.HolderAddCategory(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderAddCategory holder, int position) {
        final CategoryRoom categoryRoom =lstCategory.get(position);
        holder.txtName.setText(categoryRoom.getName() + "");
        if(categoryRoom.getImage().equals("Default")){
            holder.img_category.setImageResource(R.drawable.thumb);
        }else{
            Glide.with(context).load(categoryRoom.getImage()).into(holder.img_category);
        }
    }

    @Override
    public int getItemCount() {
        return lstCategory.size();
    }

    public class HolderAddCategory extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView img_category;
        public HolderAddCategory(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.name_group);
            img_category = itemView.findViewById(R.id.logo_item_group);
        }
    }
}

