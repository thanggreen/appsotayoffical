package com.example.appsotayenglish.ui.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.appsotayenglish.R;

import steelkiwi.com.library.DotsLoaderView;


public class MainActivity extends AppCompatActivity {

    DotsLoaderView dotsLoaderView;
   ImageView imgEnglish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dotsLoaderView = findViewById(R.id.dotsLoader);

      //  load();

        imgEnglish = findViewById(R.id.imv_laguageEnglish);
        imgEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DownLoadDataActivity.class);
                // Intent intent1 = new Intent(MainActivity.this, DownLoadDataActivity.class);
                startActivity(intent);
                //startActivity(intent1);
            }
        });
    }

//    private void load() {
//        AsyncTask<String, String, String> asyncTask = new AsyncTask<String, String, String>() {
//
//            @Override
//            protected void onPreExecute() {
//               dotsLoaderView.show();
//            }
//
//            @Override
//            protected String doInBackground(String... strings) {
//                try {
//                    Thread.sleep(5000);
//
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                return "done";
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                if(s.equals("done")){
//
//                    dotsLoaderView.hide();
//                }
//            }
//        };
//        asyncTask.execute();
//    }


}
